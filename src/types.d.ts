interface Consequence {
	stress: number;
	value: string;
}

interface MildCoreConsequence {
	stress: number;
	one: string;
	two: string;
	isMild: boolean;
	twoEnabled: boolean;
}

interface CoreHealth {
	stress: {
		physical: {
			1: boolean;
			2: boolean;
			3: boolean;
			4: boolean;
		};
		mental: {
			1: boolean;
			2: boolean;
			3: boolean;
			4: boolean;
		};
	};
	cons: {
		mild: MildCoreConsequence;
		moderate: Consequence;
		severe: Consequence;
	};
}

interface FAEHealth {
	stress: {
		1: boolean;
		2: boolean;
		3: boolean;
	};
	cons: {
		mild: Consequence;
		moderate: Consequence;
		severe: Consequence;
	};
}

declare interface SkillRecord {
	label: string;
	value: number;
	filteredSkills: SkillItem[];
}

/**
 * Data properties common to all Fate Actors
 */
declare interface FateActorData extends ActorData {
	details: {
		description: { value: string };
		biography: { value: string };
		points: {
			current: number;
			refresh: number;
		};
	};
	aspects: {
		hc: { value: string };
		trouble: { value: string };
		other: { value: string[] };
	};
}

declare interface SkillItemData extends BaseEntityData {
	data: {
		sheet: string;
		level: number;
	};
}

declare interface SkillItem extends Item {
	data: SkillItemData;
}

/**
 * Data properties common to all Fate Actor Sheets,
 * including computed properties
 */
declare interface FateActorSheetData extends ActorSheetData {
	data: FateActorData;
	stunts: any;
}

/**
 * Data properties common to all Fate Item Sheets,
 * including computed properties
 */
declare interface FateItemSheetData extends ItemSheetData {
	type: string;
	hasDetails: boolean;
	dataTemplate: Function;
	level: string;
	isCore: boolean;
	isCondensed: boolean;
	actions: Record<string, object>;
}

/**
 * Data properties used by the Fate Core Actor type
 */
declare interface CoreActorData extends FateActorData {
	health: CoreHealth;
}

/**
 * Data properties used by the Fate Accelerated (FAE) Actor type
 */
declare interface FAEActorData extends FateActorData {
	health: FAEHealth;
	approaches: {
		[key: string]: any;
	};
}

/**
 * Data properties used by the Fate Core Actor Sheet,
 * including computed properties
 */
declare interface CoreSheetData extends FateActorSheetData {
	data: CoreActorData;
	extras: Item[];
	skills: Record<string, SkillRecord>;
	physStress: any;
	mentStress: any;
	cons: {
		[key: string]: any;
	};
}

/**
 * Data properties used by the Fate Accelerated (FAE) Actor Sheet,
 * including computed properties
 */
declare interface FAEActorSheetData extends FateActorSheetData {
	data: FAEActorData;
	fateLadder: any;
	approaches: {
		[key: string]: any;
	};
	stress: any;
	cons: {
		[key: string]: any;
	};
}
